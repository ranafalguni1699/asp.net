﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public partial class Contact : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        { }
        protected void Contact_Click(object sender, EventArgs e)
        {
            // Response.Write("<script>alert('hello10')</script>");
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Insert into [ContactUs] (Message,C_Name,C_Email,Subject) values(@Message,@C_Name,@C_Email,@Subject)", con);
                //cmd.Parameters.AddWithValue("@u_Id", Session["UserId"].ToString());
                cmd.Parameters.AddWithValue("@Message", Message.Text);
                cmd.Parameters.AddWithValue("@C_Name", ContactName.Text);
                cmd.Parameters.AddWithValue("@C_Email", ContactEmail.Text);
                cmd.Parameters.AddWithValue("@Subject", ContactSubject.Text);
                cmd.ExecuteNonQuery();
                //Response.Write("<script>alert('hello1')</script>");
                cmd.Dispose();
                Response.Redirect("index.aspx");
            }
            catch (SqlException ex)
            { }
            con.Close();

        }
    }
}