﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public partial class DropdoenlistDemo : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
                SqlCommand comm = con.CreateCommand();
                comm = con.CreateCommand();
                comm.CommandText = "SELECT N_Name,N_Id from NGO";
                comm.CommandType = CommandType.Text;
                SqlDataReader dr = comm.ExecuteReader();
                while (dr.Read()){
                    DropDownList1.Items.Add(new
                    ListItem(dr[0].ToString(), dr[1].ToString()));
                }
                con.Close();
            

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue == "")
            {
                Label1.Text = "Please Select a City";
            }
            else
                Label1.Text = "Your Choice is: " + DropDownList1.SelectedValue;
        }
    }
}
