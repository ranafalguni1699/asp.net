﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara.AdminPanel
{
    public partial class NGOList : System.Web.UI.Page
    {
        SqlConnection con;
        StringBuilder htmalTable = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from NGO", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
           // DataTable dt = new DataTable();
            da.Fill(ds);
            cmd.ExecuteNonQuery();
            con.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                htmalTable.Append("<table>");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    htmalTable.Append("<tr>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["N_Name"] + "</td>");
                    htmalTable.Append("<td style = 'padding:30px'>" + ds.Tables[0].Rows[i]["N_Email"] + "</td>");
                    htmalTable.Append("<td style = 'padding:0px'>" + ds.Tables[0].Rows[i]["N_Contact"] + "</td>");
                    htmalTable.Append("<td style = 'padding-left:150px'>" + ds.Tables[0].Rows[i]["N_Address"] + "</td>");
                   // htmalTable.Append("<td>" + "<h6>" + "<a href = NGOList.aspx>" + "Edit" + "</a>" + "</h6>" + "</td>");

                    //htmalTable.Append("<td width=300px>" + ds.Tables[0].Rows[i][""] + "</td>");
                    htmalTable.Append("</tr>");
                }
                htmalTable.Append("</table>");
                DpalceHolder.Controls.Add(new Literal { Text = htmalTable.ToString() });
                htmalTable.Append("<br/>");

                con.Close();
            }
        }

    }
}






