﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Aashara.AdminPanel
{
    public partial class EventEdit : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            String eid = Request.QueryString["eid"];
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();

            SqlCommand comm1 = con.CreateCommand();
            comm1 = con.CreateCommand();
            comm1.CommandText = "SELECT N_Name,N_Id from NGO";
            comm1.CommandType = CommandType.Text;
            SqlDataReader dr1 = comm1.ExecuteReader();
            while (dr1.Read())
            {
                NGOlist.Items.Add(new
                ListItem(dr1[0].ToString(), dr1[1].ToString()));
            }
            dr1.Close();
            comm1.Dispose();

            SqlCommand comm2 = con.CreateCommand();
            comm2 = con.CreateCommand();
            comm2.CommandText = "SELECT * from Event where E_Id="+ eid+";";
            comm2.CommandType = CommandType.Text;
            SqlDataReader dr2 = comm2.ExecuteReader();
            while (dr2.Read())
            {
                EventName.Text = dr2[2].ToString();
                EventStartDate.Text = dr2[3].ToString();
                EventEndDate.Text = dr2[4].ToString();
                EventLocation.Text = dr2[5].ToString();
                EventDesc.Text = dr2[8].ToString();
                EventCity.Text = dr2[6].ToString();
                EventState.Text = dr2[7].ToString();
                NGOlist.SelectedValue= dr2[1].ToString();
            }
            dr2.Close();
            comm2.Dispose();
            con.Close();
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
           // Response.Write("<script>alert('" + NGOlist.Text + "')</script>");
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            try {  
            // Response.Write("<script>alert('" + NGOlist.SelectedValue + "')</script>");
                //Response.Write("<script>alert('" + EventName.Text + EventStartDate.Text + EventEndDate.Text + EventLocation.Text + EventCity.Text + EventState.Text + EventDesc.Text + "')</script>");
                SqlCommand cmd = new SqlCommand("update [EVENT] set NGO_Id=@NGO_Id,E_Name=@E_Name,E_Sdate=@E_Sdate,E_Edate=@E_Edate,E_Location=@E_Location,E_City=@E_City,E_State=@E_State,E_Desc=@E_Desc where E_Id=@E_id", con);
                cmd.Parameters.AddWithValue("@E_Id",Request.QueryString["eid"]);
                cmd.Parameters.AddWithValue("@NGO_Id", NGOlist.SelectedValue);
                cmd.Parameters.AddWithValue("@E_Name", EventName.Text);
                cmd.Parameters.AddWithValue("@E_Sdate", EventStartDate.Text);
                cmd.Parameters.AddWithValue("@E_Edate", EventEndDate.Text);
                cmd.Parameters.AddWithValue("@E_Location", EventLocation.Text);
                cmd.Parameters.AddWithValue("@E_City", EventCity.Text);
                cmd.Parameters.AddWithValue("@E_State", EventState.Text);
                cmd.Parameters.AddWithValue("@E_Desc", EventDesc.Text);
                //cmd.Parameters.AddWithValue("@E_Img", EventImage.PostedFile.FileName);
                Response.Write("<script>alert('error1')</script>");
                /* int rs = 0;
                 rs = (int) cmd.ExecuteNonQuery();
                 if (rs==0)
                 {
                     Response.Write("ERORR");
                 }*/
                cmd.ExecuteNonQuery();
                //cmd.Dispose();
                con.Close();
                Response.Redirect("EventList.aspx");
            }catch(Exception ex){       }
        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            Response.Redirect("EventRegister.aspx");
        }
    }
}