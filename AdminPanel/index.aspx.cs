﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara.AdminPanel
{
    public partial class index : System.Web.UI.Page
    {
        private const string Value = "Admin";
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            CountVisitors.Text = Application["OnlineUsers"].ToString();
            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
                con.Open();
                try
                {
                    SqlCommand cmd;
                    SqlDataReader dr;
                    int count = 0;
                    cmd = new SqlCommand("select * from [User]", con);
                    dr = cmd.ExecuteReader();
                    while (dr.Read()) { count += 1; }
                    //  Response.Write("<script>alert('Data 1')</script>");
                    Usercount.Text = count.ToString();
                    dr.Close();
                    cmd.Dispose();
                }
                catch (Exception ex) { }

                try
                {
                    SqlCommand cmd;
                    SqlDataReader dr;
                    int count = 0;
                    //int count2 = 0;
                    cmd = new SqlCommand("select * from NGO", con);
                    dr = cmd.ExecuteReader();
                    while (dr.Read()) { count += 1; }
                    //  Response.Write("<script>alert('Data 1')</script>");
                    CountNGO.Text = count.ToString();
                    dr.Close();
                    cmd.Dispose();
                }
                catch (Exception ex) { }

                try
                {
                    SqlCommand cmd;
                    SqlDataReader dr;
                    int count = 0;
                    cmd = new SqlCommand("select * from Event", con);
                    dr = cmd.ExecuteReader();
                    while (dr.Read()) { count += 1; }
                    //  Response.Write("<script>alert('Data 1')</script>");
                    CountEvents.Text = count.ToString();
                    dr.Close();
                    cmd.Dispose();
                }
                catch (Exception ex) { }


                con.Close();


            }
            catch (Exception ex) { }
            }
            //else{ Response.Redirect("../Login.aspx");  }
        }


    }