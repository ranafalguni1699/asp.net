﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Aashara.AdminPanel.index" %>
<!DOCTYPE html>
<!--d
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<html>
<head>
    <title>Aashara| Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- Graph CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
    <!-- lined-icons -->
    <link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
    <!-- //lined-icons -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/amcharts.js"></script>
    <script src="js/serial.js"></script>
    <script src="js/light.js"></script>
    <script src="js/radar.js"></script>
    <link href="css/barChart.css" rel='stylesheet' type='text/css' />
    <link href="css/fabochart.css" rel='stylesheet' type='text/css' />
    <!--clock init-->
    <script src="js/css3clock.js"></script>
    <!--Easy Pie Chart-->
    <!--Calender-->
    <link rel="stylesheet" href="css/clndr.css" type="text/css" />
    <script src="js/underscore-min.js" type="text/javascript"></script>
    <script src="js/moment-2.2.1.js" type="text/javascript"></script>
    <script src="js/clndr.js" type="text/javascript"></script>
    <script src="js/site.js" type="text/javascript"></script>
    <!--End Calender-->
    <!--skycons-icons-->
    <script src="js/skycons.js"></script>

    <script src="js/jquery.easydropdown.js"></script>

    <!--//skycons-icons-->
</head> 
<body>
   <div class="page-container">
   <!--/content-inner-->
	<div class="left-content">
	   <div class="inner-content">
		<!-- header-starts -->
			<div class="header-section">
						<!--menu-right-->
						<div class="top_menu">
						        <div class="main-search">
											<form>
											   <input type="text" value="Search" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Search';}" class="text"/>
												<input type="submit" value="">
											</form>
									<div class="close"><img src="images/cross.png" /></div>
								</div>
									<div class="srch"><button></button></div>
									<script type="text/javascript">
                                        $('.main-search').hide();
                                        $('button').click(function () {
                                            $('.main-search').show();
                                            $('.main-search text').focus();
                                        }
                                        );
                                        $('.close').click(function () {
                                            $('.main-search').hide();
                                        });
									</script>
							<!--/profile_details-->
								<div class="profile_details_left">
									<ul class="nofitications-dropdown">
											<li class="dropdown note dra-down">
													   <div id="dd" class="wrapper-dropdown-3" tabindex="1">
																			<span>Vadodara  </span>
																			<ul class="dropdown">
																				<li><a class="deutsch">Surat</a></li>
																				<li><a class="english"> Vadodara</a></li>
																				<li><a class="espana">Ahemdabad</a></li>
																				<li><a class="russian">Gandhinagar</a></li>

																			</ul>
																		</div>
																		<script type="text/javascript">

                                                                            function DropDown(el) {
                                                                                this.dd = el;
                                                                                this.placeholder = this.dd.children('span');
                                                                                this.opts = this.dd.find('ul.dropdown > li');
                                                                                this.val = '';
                                                                                this.index = -1;
                                                                                this.initEvents();
                                                                            }
                                                                            DropDown.prototype = {
                                                                                initEvents: function () {
                                                                                    var obj = this;

                                                                                    obj.dd.on('click', function (event) {
                                                                                        $(this).toggleClass('active');
                                                                                        return false;
                                                                                    });

                                                                                    obj.opts.on('click', function () {
                                                                                        var opt = $(this);
                                                                                        obj.val = opt.text();
                                                                                        obj.index = opt.index();
                                                                                        obj.placeholder.text(obj.val);
                                                                                    });
                                                                                },
                                                                                getValue: function () {
                                                                                    return this.val;
                                                                                },
                                                                                getIndex: function () {
                                                                                    return this.index;
                                                                                }
                                                                            }

                                                                            $(function () {

                                                                                var dd = new DropDown($('#dd'));

                                                                                $(document).click(function () {
                                                                                    // all dropdowns
                                                                                    $('.wrapper-dropdown-3').removeClass('active');
                                                                                });

                                                                            });

																</script>
										    </li>
									       <li class="dropdown note">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o"></i> <span class="badge">3</span></a>

												
													<ul class="dropdown-menu two first">
														<li>
															<div class="notification_header">
																<h3>You have 3 new messages  </h3> 
															</div>
														</li>
														<li><a href="#">
														   <div class="user_img"><img src="images/1.jpg" alt=""></div>
														   <div class="notification_desc">
															<p>Lorem ipsum dolor sit amet</p>
															<p><span>1 hour ago</span></p>
															</div>
														   <div class="clearfix"></div>	
														 </a></li>
														 <li class="odd"><a href="#">
															<div class="user_img"><img src="images/in.jpg" alt=""></div>
														   <div class="notification_desc">
															<p>Lorem ipsum dolor sit amet </p>
															<p><span>1 hour ago</span></p>
															</div>
														  <div class="clearfix"></div>	
														 </a></li>
														<li><a href="#">
														   <div class="user_img"><img src="images/in1.jpg" alt=""></div>
														   <div class="notification_desc">
															<p>Lorem ipsum dolor sit amet </p>
															<p><span>1 hour ago</span></p>
															</div>
														   <div class="clearfix"></div>	
														</a></li>
														<li>
															<div class="notification_bottom">
																<a href="#">See all messages</a>
															</div> 
														</li>
													</ul>
										</li>
										
							<li class="dropdown note">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o"></i> <span class="badge">5</span></a>

									<ul class="dropdown-menu two">
										<li>
											<div class="notification_header">
												<h3>You have 5 new notification</h3>
											</div>
										</li>
										<li><a href="#">
											<div class="user_img"><img src="images/in.jpg" alt=""></div>
										   <div class="notification_desc">
											<p>Lorem ipsum dolor sit amet</p>
											<p><span>1 hour ago</span></p>
											</div>
										  <div class="clearfix"></div>	
										 </a></li>
										 <li class="odd"><a href="#">
											<div class="user_img"><img src="images/in5.jpg" alt=""></div>
										   <div class="notification_desc">
											<p>Lorem ipsum dolor sit amet </p>
											<p><span>1 hour ago</span></p>
											</div>
										   <div class="clearfix"></div>	
										 </a></li>
										 <li><a href="#">
											<div class="user_img"><img src="images/in8.jpg" alt=""></div>
										   <div class="notification_desc">
											<p>Lorem ipsum dolor sit amet </p>
											<p><span>1 hour ago</span></p>
											</div>
										   <div class="clearfix"></div>	
										 </a></li>
										 <li>
											<div class="notification_bottom">
												<a href="#">See all notification</a>
											</div> 
										</li>
									</ul>
							</li>	
						<li class="dropdown note">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i> <span class="badge blue1">9</span></a>
										<ul class="dropdown-menu two">
										<li>
											<div class="notification_header">
												<h3>You have 9 pending task</h3>
											</div>
										</li>
										<li><a href="#">
												<div class="task-info">
												<span class="task-desc">Database update</span><span class="percentage">40%</span>
												<div class="clearfix"></div>	
											   </div>
												<div class="progress progress-striped active">
												 <div class="bar yellow" style="width:40%;"></div>
											</div>
										</a></li>
										<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
											   <div class="clearfix"></div>	
											</div>
										   
											<div class="progress progress-striped active">
												 <div class="bar green" style="width:90%;"></div>
											</div>
										</a></li>
										<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Mobile App</span><span class="percentage">33%</span>
												<div class="clearfix"></div>	
											</div>
										   <div class="progress progress-striped active">
												 <div class="bar red" style="width: 33%;"></div>
											</div>
										</a></li>
										<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
											   <div class="clearfix"></div>	
											</div>
											<div class="progress progress-striped active">
												 <div class="bar  blue" style="width: 80%;"></div>
											</div>
										</a></li>
										<li>
											<div class="notification_bottom">
												<a href="#">See all pending task</a>
											</div> 
										</li>
									</ul>
							</li>		   							   		
							<div class="clearfix"></div>	
								</ul>
							</div>
							<div class="clearfix"></div>	
							<!--//profile_details-->
						</div>
						<!--//menu-right-->
					<div class="clearfix"></div>
				</div>
					<!-- //header-ends -->
						<div class="outter-wp">
								<!--custom-widgets-->
												<div class="custom-widgets">
												   <div class="row-one">
														<div class="col-md-3 widget">
															<div class="stats-left ">
																<h4> Users</h4>
															</div>
															<div class="stats-right">
                                                                <asp:Label ID="Usercount" runat="server" Text="0" />
                                                               <!--<label>90</label>-->
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="col-md-3 widget states-mdl">
															<div class="stats-left">
																<h4>Visitors</h4>
															</div>
															<div class="stats-right">
                                                                <asp:Label ID="CountVisitors" runat="server" Text="0" ></asp:Label>
																<!--<label> 85</label>-->
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="col-md-3 widget states-thrd">
															<div class="stats-left">
																<h4>NGO</h4>
															</div>
															<div class="stats-right">
                                                                <asp:Label ID="CountNGO" runat="server" Text="0"> </asp:Label>
																<!--<label>51</label>-->
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="col-md-3 widget states-last">
															<div class="stats-left">
																 <h4>Events</h4>
															</div>
															<div class="stats-right">
                                                                <asp:Label ID="CountEvents" runat="server" Text="0"></asp:Label>
																<!--<label>30</label>-->
															</div>
															<div class="clearfix"> </div>	
														</div>
														<div class="clearfix"> </div>	
													</div>
												</div>
												<!--//custom-widgets-->
												<!--/candile
													<div class="candile"> 
															<div class="candile-inner">
																	<h3 class="sub-tittle">Candlestick Chart </h3>
															    <div id="center"><div id="fig">
																<script type="text/javascript+protovis">

															/* Parse dates. */
															var dateFormat = pv.Format.date("%d-%b-%y");
															vix.forEach(function(d) d.date = dateFormat.parse(d.date));

															/* Scales. */
															var w =1220,
																h = 300,
																x = pv.Scale.linear(vix, function(d) d.date).range(0, w),
																y = pv.Scale.linear(vix, function(d) d.low, function(d) d.high).range(0, h).nice();

															var vis = new pv.Panel()
																.width(w)
																.height(h)
																.margin(10)
																.left(30);

															/* Dates. */
															vis.add(pv.Rule)
																 .data(x.ticks())
																 .left(x)
																 .strokeStyle("#eee")
															   .anchor("bottom").add(pv.Label)
																 .text(x.tickFormat);

															/* Prices. */
															vis.add(pv.Rule)
																 .data(y.ticks(7))
																 .bottom(y)
																 .left(-10)
																 .right(-10)
																 .strokeStyle(function(d) d % 10 ? "#ddd" : "#ddd")
															   .anchor("left").add(pv.Label)
																 .textStyle(function(d) d % 10 ? "#999" : "#ddd")
																 .text(y.tickFormat);

															/* Candlestick. */
															vis.add(pv.Rule)
																.data(vix)
																.left(function(d) x(d.date))
																.bottom(function(d) y(Math.min(d.high, d.low)))
																.height(function(d) Math.abs(y(d.high) - y(d.low)))
																.strokeStyle(function(d) d.open < d.close ? "#052963" : "#00C6D7")
															  .add(pv.Rule)
																.bottom(function(d) y(Math.min(d.open, d.close)))
																.height(function(d) Math.abs(y(d.open) - y(d.close)))
																.lineWidth(10);

															vis.render();

																</script>
																	<script type="text/javascript" src="js/protovis-d3.2.js"></script>
																	<script type="text/javascript" src="js/vix.js"></script>

															</div>
														</div>
																				
															</div>
															
														</div>
													//candile-->
													
												<!--/charts-->
						 <div class="charts">
						   <div class="chrt-inner">
                               <div class="area-charts">
                                   <!--Calander Start -->
                                   <div class="cal-main">
                                       <div class="calender graph-form">
                                           <h2 class="inner-tittle">Calender</h2>
                                           <div class="cal1">
                                               <div class="clndr">
                                                   <div class="clndr-controls">
                                                       <div class="clndr-control-button"><span class="clndr-previous-button">previous</span></div>
                                                       <div class="month">October 2019</div>
                                                       <div class="clndr-control-button rightalign"><span class="clndr-next-button">next</span></div>
                                                   </div>
                                                   <table class="clndr-table" border="0" cellspacing="0" cellpadding="0">
                                                       <thead>
                                                           <tr class="header-days">
                                                               <td class="header-day">S</td>
                                                               <td class="header-day">M</td>
                                                               <td class="header-day">T</td>
                                                               <td class="header-day">W</td>
                                                               <td class="header-day">T</td>
                                                               <td class="header-day">F</td>
                                                               <td class="header-day">S</td>
                                                           </tr>
                                                       </thead>
                                                       <tbody>
                                                           <tr>
                                                               <td class="day past adjacent-month last-month calendar-day-2019-09-29 calendar-dow-0">
                                                                   <div class="day-contents">29</div>
                                                               </td>
                                                               <td class="day past adjacent-month last-month calendar-day-2019-09-30 calendar-dow-1">
                                                                   <div class="day-contents">30</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-01 calendar-dow-2">
                                                                   <div class="day-contents">1</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-02 calendar-dow-3">
                                                                   <div class="day-contents">2</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-03 calendar-dow-4">
                                                                   <div class="day-contents">3</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-04 calendar-dow-5">
                                                                   <div class="day-contents">4</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-05 calendar-dow-6">
                                                                   <div class="day-contents">5</div>
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td class="day past calendar-day-2019-10-06 calendar-dow-0">
                                                                   <div class="day-contents">6</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-07 calendar-dow-1">
                                                                   <div class="day-contents">7</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-08 calendar-dow-2">
                                                                   <div class="day-contents">8</div>
                                                               </td>
                                                               <td class="day past calendar-day-2019-10-09 calendar-dow-3">
                                                                   <div class="day-contents">9</div>
                                                               </td>
                                                               <td class="day past event calendar-day-2019-10-10 calendar-dow-4">
                                                                   <div class="day-contents">10</div>
                                                               </td>
                                                               <td class="day past event calendar-day-2019-10-11 calendar-dow-5">
                                                                   <div class="day-contents">11</div>
                                                               </td>
                                                               <td class="day today event calendar-day-2019-10-12 calendar-dow-6">
                                                                   <div class="day-contents">12</div>
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td class="day event calendar-day-2019-10-13 calendar-dow-0">
                                                                   <div class="day-contents">13</div>
                                                               </td>
                                                               <td class="day event calendar-day-2019-10-14 calendar-dow-1">
                                                                   <div class="day-contents">14</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-15 calendar-dow-2">
                                                                   <div class="day-contents">15</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-16 calendar-dow-3">
                                                                   <div class="day-contents">16</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-17 calendar-dow-4">
                                                                   <div class="day-contents">17</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-18 calendar-dow-5">
                                                                   <div class="day-contents">18</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-19 calendar-dow-6">
                                                                   <div class="day-contents">19</div>
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td class="day calendar-day-2019-10-20 calendar-dow-0">
                                                                   <div class="day-contents">20</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-21 calendar-dow-1">
                                                                   <div class="day-contents">21</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-22 calendar-dow-2">
                                                                   <div class="day-contents">22</div>
                                                               </td>
                                                               <td class="day event calendar-day-2019-10-23 calendar-dow-3">
                                                                   <div class="day-contents">23</div>
                                                               </td>
                                                               <td class="day event calendar-day-2019-10-24 calendar-dow-4">
                                                                   <div class="day-contents">24</div>
                                                               </td>
                                                               <td class="day event calendar-day-2019-10-25 calendar-dow-5">
                                                                   <div class="day-contents">25</div>
                                                               </td>
                                                               <td class="day event calendar-day-2019-10-26 calendar-dow-6">
                                                                   <div class="day-contents">26</div>
                                                               </td>
                                                           </tr>
                                                           <tr>
                                                               <td class="day calendar-day-2019-10-27 calendar-dow-0">
                                                                   <div class="day-contents">27</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-28 calendar-dow-1">
                                                                   <div class="day-contents">28</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-29 calendar-dow-2">
                                                                   <div class="day-contents">29</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-30 calendar-dow-3">
                                                                   <div class="day-contents">30</div>
                                                               </td>
                                                               <td class="day calendar-day-2019-10-31 calendar-dow-4">
                                                                   <div class="day-contents">31</div>
                                                               </td>
                                                               <td class="day adjacent-month next-month calendar-day-2019-11-01 calendar-dow-5">
                                                                   <div class="day-contents">1</div>
                                                               </td>
                                                               <td class="day adjacent-month next-month calendar-day-2019-11-02 calendar-dow-6">
                                                                   <div class="day-contents">2</div>
                                                               </td>
                                                           </tr>
                                                       </tbody>
                                                   </table>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                                  <!--Calander End -->
                                       <br />
                                       <!--/bottom-grids-->
                                       <div class="bottom-grids">
                                           <div class="dev-table">
                                               <div class="col-md-4 dev-col">

                                                   <div class="dev-widget dev-widget-transparent">
                                                       <h2 class="inner one">Server Usage</h2>
                                                       <p>Today server usage in percentages</p>
                                                       <div class="dev-stat"><span class="counter">68</span>%</div>
                                                       <div class="progress progress-bar-xs">
                                                           <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                                       </div>
                                                       <p>We Todayly recommend you change your plan to <strong>Pro</strong>. Click here to get more details.</p>

                                                       <a href="#" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>
                                                   </div>

                                               </div>
                                               <div class="col-md-4 dev-col mid">

                                                   <div class="dev-widget dev-widget-transparent dev-widget-success">
                                                       <h3 class="inner">Today Donation</h3>
                                                       <p>This is Today Donation per last year</p>
                                                       <div class="dev-stat">$<span class="counter">75,332</span></div>
                                                       <div class="progress progress-bar-xs">
                                                           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 79%;"></div>
                                                       </div>
                                                       <p>We happy to notice you that you become an Elite customer, and you can get some custom sugar.</p>

                                                       <a href="#" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>
                                                   </div>

                                               </div>
                                               <div class="col-md-4 dev-col">

                                                   <div class="dev-widget dev-widget-transparent dev-widget-danger">
                                                       <h3 class="inner">Yearly Donation</h3>
                                                       <p>All your earnings for this time</p>
                                                       <div class="dev-stat">$<span class="counter">5,321</span></div>
                                                       <div class="progress progress-bar-xs">
                                                           <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                                                       </div>
                                                       <p>You can withdraw this money in end of each month. Also you can spend it on our marketplace.</p>

                                                       <a href="#" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a>
                                                   </div>

                                               </div>
                                               <div class="clearfix"></div>

                                           </div>
                                       </div>
                                       <!--//bottom-grids-->

                                   </div>
                                   <!--/charts-inner-->
                               </div>
										<!--//outer-wp-->
									</div>
									 <!--footer section start-->
										<footer>
										   <p>&copy 2019 Aashara . All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">W3layouts.</a></p>
										</footer>
									<!--footer section end-->
								</div>
							</div>
				<!--//content-inner-->
			<!--/sidebar-menu-->
				<div class="sidebar-menu">
					<header class="logo">
					<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="index.aspx"> <span id="logo">
                        <h1>Admin</h1></span> 
					<!--<img id="logo" src="" alt="Logo"/>--> 
				  </a> 
				  </header>
			      <div style="border-top:1px solid rgba(69, 74, 84, 0.7)"></div>
			                   <!--/down-->
							<div class="down">	
									  <a href="index.aspx"><img src="images/admin.jpg"></a>
									  <a href="index.aspx"><span class=" name-caret">Jasmin Leo</span></a>
									 <p>System Administrator in Aashara</p>
									  <ul>
									    <li><a class="tooltips" href="index.aspx"><span>Profile</span><i class="lnr lnr-user"></i></a></li>
										<li><a class="tooltips" href="index.aspx"><span>Settings</span><i class="lnr lnr-cog"></i></a></li>
										<li><a class="tooltips" href="../Login.aspx"><span>Log out</span><i class="lnr lnr-power-switch"></i></a></li>
									  </ul>
									</div>
							   <!--//down-->
                           <br/>
                           <div class="menu">
                               <ul id="menu">
                                   <li><a href="index.aspx"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
                                   <br />
                                   <li><a href="NGOList.aspx"><i class="lnr lnr-pencil"></i> <span>NGOs</span></a></li>
                                   <br />
                                   <li id="menu-academico">
                                       <a href="#"><i class="fa fa-file-text-o"></i> <span>Events</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                                       <ul id="menu-academico-sub">
                                           <li id="menu-academico-avaliacoes"><a href="EventRegister.aspx">Registered</a></li>
                                           <li id="menu-academico-boletim"><a href="EventList.aspx">List</a></li>
                                       </ul>
                                   </li>
                                   <br />
                                   <li><a href="UserList.aspx"><i class="lnr lnr-pencil"></i> <span>User</span></a></li>
                               </ul>
						   </div>
						</div>
					<div class="clearfix"></div>		
	</div>
							<script>
                                var toggle = true;

                                $(".sidebar-icon").click(function () {
                                    if (toggle) {
                                        $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                                        $("#menu span").css({ "position": "absolute" });
                                    }
                                    else {
                                        $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                                        setTimeout(function () {
                                            $("#menu span").css({ "position": "relative" });
                                        }, 400);
                                    }

                                    toggle = !toggle;
                                });
							</script>
<!--js -->
<link rel="stylesheet" href="css/vroom.css">
<script type="text/javascript" src="js/vroom.js"></script>
<script type="text/javascript" src="js/TweenLite.min.js"></script>
<script type="text/javascript" src="js/CSSPlugin.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>