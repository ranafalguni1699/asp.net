﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara.AdminPanel
{
    public partial class UserList : System.Web.UI.Page
    {
        SqlConnection con;
        StringBuilder htmalTable = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from [User]", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            da.Fill(ds);
            cmd.ExecuteNonQuery();
            con.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                htmalTable.Append("<table height=200px, width=1080px>");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    htmalTable.Append("<tr>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["U_Name"] + "</td>");
                    htmalTable.Append("<td style = 'padding:5px'>" + ds.Tables[0].Rows[i]["U_Email"] + "</td>");
                    htmalTable.Append("<td style = 'padding:5px'>" + ds.Tables[0].Rows[i]["U_Contact_No"] + "</td>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["U_gender"] + "</td>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["U_DOB"] + "</td>");
                    htmalTable.Append("<td style = 'padding:5px'>" + ds.Tables[0].Rows[i]["U_Address"] + "</td>");
                    htmalTable.Append("<td style = 'padding:5px'>" + ds.Tables[0].Rows[i]["U_City"] + "</td>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["U_State"] + "</td>");
                   // htmalTable.Append("<td>" + "<h6>" + "<a href = UserList.aspx>" + "Edit" + "</a>" + "</h6>" + "</td>");
                    htmalTable.Append("</tr>");


                }
                htmalTable.Append("</table>");
                DBDataPlaceHolder.Controls.Add(new Literal { Text = htmalTable.ToString() });
                htmalTable.Append("<br/>");

            }
            /*foreach (DataRow dr in dt.Rows)
            {
                string name = dr["U_name"].ToString();
                string email = dr["U_Email"].ToString();
                string address = dr["U_DOB"].ToString();
                string gender = dr["U_gender"].ToString();
                string city = dr["U_City"].ToString();
                string state = dr["U_State"].ToString();
                string contact = dr["U_Contact_No"].ToString();
                //htmlTable += "<tr><td>" + id + "</td><td>" + name + "</td><td>" + country + "</td></tr>";
            }*/



            con.Close();
        }
    }
}