﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara.AdminPanel
{
    public partial class EventList : System.Web.UI.Page
    {
        SqlConnection con;
        StringBuilder htmalTable = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from Event", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            da.Fill(ds);
            cmd.ExecuteNonQuery();
            con.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                htmalTable.Append("<table>");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    htmalTable.Append("<tr>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["E_Name"] + "</td>");
                    htmalTable.Append("<td  style = 'padding:60px'>" + ds.Tables[0].Rows[i]["NGO_Id"] + "</td>");
                    htmalTable.Append("<td  style = 'padding:20px'>" + ds.Tables[0].Rows[i]["E_Sdate"] + "</td>");
                    htmalTable.Append("<td style = 'padding:0px'>" +  ds.Tables[0].Rows[i]["E_Edate"] + "</td>");
                    htmalTable.Append("<td style = 'padding:30px'>" + ds.Tables[0].Rows[i]["E_Location"] + "</td>");
                    htmalTable.Append("<td style = 'padding:50px'>" + ds.Tables[0].Rows[i]["E_City"] + "</td>");
                    htmalTable.Append("<td style = 'padding:10px'>" + ds.Tables[0].Rows[i]["E_State"] + "</td>");
                    htmalTable.Append("<td style = 'padding:10px'><a href='EventEdit.aspx?eid=" + ds.Tables[0].Rows[i]["E_ID"] + "'>Edit</a></td>");
                    //con.Open();
                    /* SqlCommand cmd1 = new SqlCommand("select * from volunteer ", con);
                     SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                     //int volunteercount=Convert.ToInt32(cmd.ExecuteScalar());
                     DataSet ds1 = new DataSet();
                     DataTable dt1 = new DataTable();
                     da.Fill(ds1);
                     con.Open();
                     cmd.ExecuteNonQuery();
                     con.Close();
                     try
                     {
                         if (ds1.Tables[0].Rows.Count > 0)
                         {
                             for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                             {
                                // htmalTable.Append("<td width=100px>" + ds1.Tables[0].Rows[j]["V_Id"] + "</td>");
                             }
                         }
                     }
                     catch (Exception ex)
                     { }*/
                    htmalTable.Append("</tr>");
                }
                htmalTable.Append("</table>");
                DBpalceHolder.Controls.Add(new Literal { Text = htmalTable.ToString() });
                htmalTable.Append("<br/>");
                con.Close();
            }
        }
    }
}






















