﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara.AdminPanel
{
    public partial class EventRegister : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            SqlCommand comm = con.CreateCommand();
            comm = con.CreateCommand();
            comm.CommandText = "SELECT N_Name,N_Id from NGO";
            comm.CommandType = CommandType.Text;
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                NGOlist.Items.Add(new
                ListItem(dr[0].ToString(), dr[1].ToString()));
            }
            con.Close();
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('"+NGOlist.Text+"')</script>");
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            Response.Write("<script>alert('hello9')</script>");
            StringBuilder sb = new StringBuilder();
            try
            {
                if (EventImage.HasFile)
                {
                    if ((EventImage.PostedFile != null) && (EventImage.PostedFile.ContentLength > 0))
                    {
                        string fn = System.IO.Path.GetFileName(EventImage.PostedFile.FileName);
                        string SaveLocation = Server.MapPath("Upload") + "\\" + fn;
                        try
                        {
                            EventImage.PostedFile.SaveAs(SaveLocation);
                        }
                        catch (Exception ex) {
                            Response.Write("<script>alert('" + ex.StackTrace + ex.Message + "')</script>");
                        }
                    }
                }
               Response.Write("<script>alert('"+NGOlist.SelectedValue+"')</script>");
               //Response.Write("<script>alert('" + EventName.Text + EventStartDate.Text + EventEndDate.Text + EventLocation.Text + EventCity.Text + EventState.Text + EventDesc.Text + "')</script>");
                SqlCommand cmd = new SqlCommand("Insert into [EVENT] (NGO_Id,E_Name,E_Sdate,E_Edate,E_Location,E_City,E_State,E_Desc,E_Img) values(@NGO_Id,@E_Name,@E_Sdate,@E_Edate,@E_Location,@E_City,@E_State,@E_Desc,@E_Img)", con);
                cmd.Parameters.AddWithValue("@NGO_Id", NGOlist.SelectedValue);
                cmd.Parameters.AddWithValue("@E_Name", EventName.Text);
                cmd.Parameters.AddWithValue("@E_Sdate", EventStartDate.Text);
                cmd.Parameters.AddWithValue("@E_Edate", EventEndDate.Text);
                cmd.Parameters.AddWithValue("@E_Location", EventLocation.Text);
                cmd.Parameters.AddWithValue("@E_City", EventCity.Text);
                cmd.Parameters.AddWithValue("@E_State", EventState.Text);
                cmd.Parameters.AddWithValue("@E_Desc", EventDesc.Text);
                cmd.Parameters.AddWithValue("@E_Img", EventImage.PostedFile.FileName);
                // Response.Write("<script>alert('error1')</script>");
                /* int rs = 0;
                 rs = (int) cmd.ExecuteNonQuery();
                 if (rs==0)
                 {
                     Response.Write("ERORR");
                 }*/
                cmd.ExecuteNonQuery();
                //cmd.Dispose();
                con.Close();
                Response.Redirect("EventList.aspx");
            }
            catch(Exception ex) {
                
            }
        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            Response.Redirect("EventRegister.aspx");
        }
    }
}