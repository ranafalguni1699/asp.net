﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NGORegister.aspx.cs" Inherits="Aashara.NGORegister" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aashara</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- magnific popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- nice select CSS -->
    <link rel="stylesheet" href="css/nice-select.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
   <form id="forms" runat="server">
    <!--::header part start::-->
  <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.aspx"> <img src="img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.aspx">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="NGO.aspx">NGOs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="Gallery.aspx">Gallery</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="Event.aspx">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="Contact.aspx">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="About.aspx">about</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="Login.aspx">Login</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
    </header>
    <!-- Header part end-->
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item text-center">
                            <h2>NGO Registration</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->
    <!-- Start Sample Area -->
    
    <!-- End Sample Area -->
    <!-- Start Button -->
    
    <!-- End Button -->
    <!-- Start Align Area -->
     <!--Form End-->
    
     <div class="whole-wrap">
        <div class="container box_1170">
            <div class="section-top-border">
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <h3 class="mb-30">NGO Registration Form</h3>
                       <form id="UserRegister" method="post">
                            <div class="mt-10">
                                <asp:TextBox runat="server" ID="NGOName" placeholder="NGO Name" CssClass="single-input-primary" TextMode="SingleLine"/>
                                <asp:RequiredFieldValidator ID="NGONameRFV" runat="server" 
                                     ControlToValidate ="NGOName"
                                     ErrorMessage="Please Enter your First name" />
                            </div>
                          
                            <div class="mt-10">
                                <asp:TextBox ID="NGOEmail" runat="server" TextMode="SingleLine" CssClass="single-input-primary" 
                                 placeholder="Email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'"/>
                                <asp:RegularExpressionValidator ID="NGOEmailREV" runat="server" 
                                     ControlToValidate ="NGOEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                     ErrorMessage="Please Enter your Email" />
                            </div>
                            <div class="mt-10"> 
                            <asp:TextBox ID="NGOPswd" runat="server" TextMode="Password" CssClass="single-input-primary"
                                placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'"/>
                               <asp:RegularExpressionValidator ID="NGOPswdREV" runat="server" 
                                   ControlToValidate ="NGOPswd" ValidationExpression="^[a-zA-Z0-9'@&#.\s]{7,10}$"  
                                   ErrorMessage="Password length must be between 7 to 10 characters" />
                            </div>
                            <div class="mt-10">
                               <asp:TextBox ID="NGOconfirmpswd" runat="server" TextMode="Password" CssClass="single-input-primary"
                                placeholder=" Confirm Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirm Password'"/>
                               <asp:RegularExpressionValidator ID="NGOconfirmpswdREV" runat="server" ControlToValidate ="NGOconfirmpswd" ValidationExpression="^[a-zA-Z0-9'@&#.\s]{7,10}$" ErrorMessage="Password is not match" />
                            </div>
                            <div class="mt-10">
                                 <asp:TextBox ID="NGOContact" runat="server" TextMode="SingleLine" CssClass="single-input-primary" 
                                     placeholder="Contact Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Contact Number'"/>
                                 <asp:RegularExpressionValidator ID="NGOContactREV" runat="server" 
                                   ControlToValidate ="NGOContact" ValidationExpression="^((\+1)?[\s-]?)?\(?[2-9]\d\d\)?[\s-]?[2-9]\d\d[\s-]?\d\d\d\d"  
                                   ErrorMessage="Please Enter a Valid Contact Number"/>
                            </div>
                          
                        
                            <div class="input-group-icon mt-10">
                                <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                <asp:TextBox ID="NGOAddress" runat="server" TextMode="SingleLine" CssClass="single-input-primary" 
                                    placeholder="Address" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Address'" />
                                <asp:RequiredFieldValidator ID="NGOAddressRFV" runat="server" 
                                   ControlToValidate ="NGOAddress"
                                   ErrorMessage="Please Enter Address" />                                                    
                            </div>

                           <div class="input-group-icon mt-10">
                                <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                <asp:TextBox ID="NGODescription" runat="server" TextMode="MultiLine" CssClass="single-input-primary" 
                                    placeholder="Description" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Address'" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                   ControlToValidate ="NGODescription"
                                   ErrorMessage="Please Enter NGO Description" />                                                    
                            </div>
                            <div class="input-group-icon mt-40"> 
                               <table>
                                   <tr>
                             <td><div class="single-input-primary">
                                <asp:Label runat="server" ID="lblimg" Text="NGO Certificate"></asp:Label><br />
                                <asp:FileUpload runat="server" ID="NGOCertificate" />
                            </div>   
                           </td>
                           <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>                             
                           <td> <div class="single-input-primary">
                                <asp:Label runat="server" ID="Label1" Text="NGO Image"></asp:Label><br /> 
                                <asp:FileUpload runat="server" ID="NGOImg" />
                            </div>   
                           </td>
                                   </tr>

                               </table>
                            </div> 
                           <!--<div class="mt-10">
                              <asp:TextBox ID="desc" runat="server" TextMode="MultiLine" class="single-textarea single-input-primary" placeholder="Description" onfocus="this.placeholder = ''"
                                onblur="this.placeholder = 'Message'" />
                            </div>-->
                            <div class="button-group-area mt-40"><h3>
                              <asp:Button ID="submit" runat="server" OnClick="Submit_Click" Text="Submit" CssClass="genric-btn primary e-large"/>
                              <asp:Button ID="reset" runat="server" OnClick="Reset_Click" Text="Reset" CssClass="genric-btn danger e-large" /></h3>
                           </div>
                          
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  <%--</form>--%>
  <!--Form End-->
    
    <!-- End Align Area -->
    <!-- footer part start-->
    <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.aspx"> <img src="img/logo.png" alt=""> </a>
                        <p>NGOs is expanded to nongovernment organizations. 
                            They are thus a subgroup of all organizations founded by citizens, which include clubs and other associations that provide services, benefits, and premises only to members. 
                            </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Newsletter</h4>
                        <p>Stay updated with our latest trends Seed heaven so said place winged over given forth fruit.
                        </p>
                        <div class="form-wrap" id="mc_embed_signup">
                            <form target="_blank"
                                action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="form-inline">
                                <input class="form-control" name="EMAIL" placeholder="Your Email Address"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
                                    required="" type="email">
                                <button class="btn btn-default text-uppercase"><i class="ti-angle-right"></i></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value=""
                                        type="text">
                                </div>

                                <div class="info"></div>
                            </form>
                        </div>
                        <div class="social_icon">
                            <a href="#"> <i class="ti-facebook"></i> </a>
                            <a href="#"> <i class="ti-twitter-alt"></i> </a>
                            <a href="#"> <i class="ti-instagram"></i> </a>
                            <a href="#"> <i class="ti-skype"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contact us</h4>
                        <p>Faculty of technology and engineering, Near badamani baug , Kalabhavan,Vadodara,390002</p>
                        <div class="contact_info">
                            <p><span class="ti-mobile"></span> +91-8200680204</p>
                            <p><span class="ti-email"></span>MSU.ac.in</p>
                            <p><span class="ti-world"></span>Aashara.com </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->
    <!-- jquery plugins here-->

    <script src="js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="js/swiper.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.smooth-scroll.min.js"></script>
    <!-- swiper js -->
    <script src="js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- contact js -->
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/contact.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</form>
</body>
</html>
