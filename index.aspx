﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Aashara.index" %>

<!doctype html>
<html lang="en">

<head runat="server">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aashara</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- magnific popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- nice select CSS -->
    <link rel="stylesheet" href="css/nice-select.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body runat="server">
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.aspx"> <img src="img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.aspx">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="NGO.aspx">NGOs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="Gallery.aspx">Gallery</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="Event.aspx">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="Contact.aspx">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="About.aspx">about</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="Login.aspx">Login</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 offset-lg-5">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h1>Helping The Destitute People!</h1>
                            <p>Support these NGOs to uplift the poor by providing them food, Shelter, education and livelihood. Donate to an NGO in Gujarat now!....</p>
                            <a href="Donation.aspx" class="btn_2">Start Donation</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- feature_part start-->
    <section class="feature_part">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <p>Awesome Feature</p>
                        <h2>How Could You Help </h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-5 col-sm-6">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <div class=" d-flex align-items-center">
                                <img src="img/icon/feature_1.svg" alt="">
                                <h4>Give Donation</h4>
                            </div>
                            <p>Support Women's rights and help a woman in need to raise her voice against discrimination. Donate to an NGO for women and make india safer for women!.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-6">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <div class=" d-flex align-items-center">
                                <img src="img/icon/feature_2.svg" alt="">
                                <a href="Event.aspx"><h4>Become A Volunteer</h4></a>
                            </div>
                            <p>Thank you for your interest in volunteering with Make-A-Wish @Aashara. We need volunteers in large numbers..!</p>
 <!--<p>At Aashara you can get support from professionally trained and skilled volunteers. The volunteers are selected with great care, chosen as they are for their ability to listen with empathy and understanding. Each volunteer has to undergo training for a minimum of six months before they can start dealing with callers.
Suicide prevention is Everybody's Responsibilty, Join Us to make a Difference</p>    -->    <br/>               
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-6">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <div class=" d-flex align-items-center">
                                <img src="img/icon/feature_3.svg" alt="">
                                <h4>Events</h4>
                            </div>
                            <p>Events are basically provide by NGOs. User can Participation in particular events 
                                and also as part of events.so Events gernally give opportunity to the user.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-6">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <div class=" d-flex align-items-center">
                                <img src="img/icon/feature_4.svg" alt="">
                                <h4>NGOs</h4>
                            </div>
                            <p>NGO [Non Government Organizations] activities in education,health, agriculture, community development,
                                energy,environment, and waste,moral upbrining.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature_part start-->

    <!-- top_service part start-->
    <section class="be_part">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6">
                    <div class="be_part_text">
                        <h2>Be a part of the break
                            through and make someones
                            dream come true</h2>
                        <p>Women are treated as second class citizens in india. They need support to stand up on their own feet. With increased awareness, more organizations have taken a step forward to help women in india. These GiveAssured NGOs are doing their bit to help women in india..</p>
                        <a href="About.aspx" class="btn_2">learn more</a>
                    </div>
                </div>
            </div>
        </div>
        <img src="img/Charity.jpg" alt="" class="be_img">
    </section>
    <!-- top_service part end-->

    <!-- counter part start-->
    <section class="counter">
        <form runat="server">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="single_counter d-flex">
                        <img src="img/icon/feature_1.svg" alt="">
                        <div class="single_counter_text">
                            <p>Total Collection</p>
                            <span class="count">58,012</span>
                        </div>
                    </div>
                    <div class="single_counter d-flex">
                        <img src="img/icon/feature_2.svg" alt="">
                        <div class="single_counter_text">
                            <p>NGOs</p>
                            <span class="count"><asp:Label  runat="server" ID="NCount"/></span>
                        </div>
                    </div>
                    <div class="single_counter d-flex">
                        <img src="img/icon/feature_3.svg" alt="">
                        <div class="single_counter_text">
                            <p>Total Volunteer</p>
                            <span class="count"><asp:Label runat="server" ID="VCount"/></span>
                        </div>
                    </div>
                    <div class="single_counter d-flex">
                        <img src="img/icon/feature_4.svg" alt="">
                        <div class="single_counter_text">
                            <p>Events</p>
                            <span class="count"><asp:Label  runat="server" ID="Ecount"/></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </form>
    </section>
    <!-- counter part end-->

    <!--::passion part start::-->
    <section class="passion_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <p>Donation shows Passion</p>
                        <h2>Upcoming Events</h2>
                    </div>
                </div>
            </div>
            <div class="row" id="EventDiv" runat="server">
                <!--Event List-->
            </div>
        </div>
    </section>
    <!--::passion part end::-->

    <!-- intro_video_bg start-->
    <section class="intro_video_bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="intro_video_iner text-center">
                        <h2>Forget what you can get and
                            see what you can give</h2>
                        <a href="Event.aspx" class="btn_2">Become a Volunteer</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- intro_video_bg part start-->

    <!--::volunteers_part start::-->
    <section class="volunteers_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <p>volunteers</p>
                        <h2>Expert Volunteers</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="single_blog_item">
                        <div class="single_blog_img">
                            <img src="img/volunteers/volunteers_1.png"  alt="doctor">
                            <div class="social_icon">
                                <a href="#"> <i class="ti-facebook"></i> </a>
                                <a href="#"> <i class="ti-twitter-alt"></i> </a>
                                <a href="#"> <i class="ti-instagram"></i> </a>
                                <a href="#"> <i class="ti-skype"></i> </a>
                            </div>
                        </div>
                        <div class="single_text">
                            <div class="single_blog_text">
                                <h3>Denish vala</h3>
                                <p>Project Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_blog_item">
                        <div class="single_blog_img">
                            <img src="img/volunteers/volunteers_2.png" alt="doctor">
                            <div class="social_icon">
                                <a href="#"> <i class="ti-facebook"></i> </a>
                                <a href="#"> <i class="ti-twitter-alt"></i> </a>
                                <a href="#"> <i class="ti-instagram"></i> </a>
                                <a href="#"> <i class="ti-skype"></i> </a>
                            </div>
                        </div>
                        <div class="single_text">
                            <div class="single_blog_text">
                                <h3>Lindsa Rudolph</h3>
                                <p>Field Supervisor</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_blog_item">
                        <div class="single_blog_img">
                            <img src="img/volunteers/volunteers_3.png" alt="doctor">
                            <div class="social_icon">
                                <a href="#"> <i class="ti-facebook"></i> </a>
                                <a href="#"> <i class="ti-twitter-alt"></i> </a>
                                <a href="#"> <i class="ti-instagram"></i> </a>
                                <a href="#"> <i class="ti-skype"></i> </a>
                            </div>
                        </div>
                        <div class="single_text">
                            <div class="single_blog_text">
                                <h3>Samuel Gardner</h3>
                                <p>Co Founder</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_blog_item">
                        <div class="single_blog_img">
                            <img src="img/volunteers/volunteers_4.png" alt="doctor">
                            <div class="social_icon">
                                <a href="#"> <i class="ti-facebook"></i> </a>
                                <a href="#"> <i class="ti-twitter-alt"></i> </a>
                                <a href="#"> <i class="ti-instagram"></i> </a>
                                <a href="#"> <i class="ti-skype"></i> </a>
                            </div>
                        </div>
                        <div class="single_text">
                            <div class="single_blog_text">
                                <h3>Lindsa Rudolph</h3>
                                <p>Field Supervisor</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::volunteers_part end::-->

    <!--::blog_part start::-->
    <section class="blog_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <p>OUr blog</p>
                        <h2>Every Single Update</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single_blog">
                        <div class="appartment_img">
                            <img src="img/blog_1.png" alt="">
                        </div>
                        <div class="single_appartment_content">
                            <a href="blog.html">
                                <h4>GOONJ is an award-winning social enterprise which has for the last 19 years has established a culture of mindful giving in urban india.
                                </h4>
                            </a>
                            <ul class="list-unstyled">
                                <li><a href=""> <span class="flaticon-calendar"></span> </a> Founded 1999</li>
                                <li><a href=""> <span class="flaticon-comment"></span> </a> GOONJ NGO,New Delhi,india</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right_single_blog">
                        <div class="single_blog">
                            <div class="media">
                                <div class="media-body align-self-center">
                                    <p><a href="#">GOONJ</a></p>
                                    <a href="blog.html">
                                        <h5 class="mt-0"> Excellent Service Organization..</h5>
                                    </a>
                                    <ul class="list-unstyled">
                                        <li><a href=""> <span class="flaticon-calendar"></span> </a> April 10, 2018</li>
                                        <li><a href=""> <span class="flaticon-comment"></span> </a> 10 comments</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="single_blog">
                            <div class="media">
                                <div class="media-body align-self-center">
                                    <p><a href="#">GOONJ</a></p>
                                    <a href="blog.html">
                                        <h5 class="mt-0"> Reuse, Recycle, Save environment and doing good service in villages. </h5>
                                    </a>
                                    <ul class="list-unstyled">
                                        <li><a href=""> <span class="flaticon-calendar"></span> </a> Oct 22, 2019</li>
                                        <li><a href=""> <span class="flaticon-comment"></span> </a> 2 comments</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="single_blog">
                            <div class="media">
                                <div class="media-body align-self-center">
                                    <p><a href="#">GOONJ</a></p>
                                    <a href="blog.html">
                                        <h5 class="mt-0"> An excellent NGO working in remote rural areas. </h5>
                                    </a>
                                    <ul class="list-unstyled">
                                        <li><a href=""> <span class="flaticon-calendar"></span> </a> May 10, 2019</li>
                                        <li><a href=""> <span class="flaticon-comment"></span> </a> 1 comments</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!--::our client part start::-->
    <section class="client_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <p>OUr Client</p>
                        <h2>Worldwide Partners</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="client_logo owl-carousel">
                        <div class="single_client_logo">
                            <img src="img/client_logo/Logo_1.png" alt="">
                        </div>
                        <div class="single_client_logo">
                            <img src="img/client_logo/Logo_2.png" alt="">
                        </div>
                        <div class="single_client_logo">
                            <img src="img/client_logo/Logo_3.png" alt="">
                        </div>
                        <div class="single_client_logo">
                            <img src="img/client_logo/Logo_4.png" alt="">
                        </div>
                        <div class="single_client_logo">
                            <img src="img/client_logo/Logo_5.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::our client part end::-->

    <!-- footer part start-->
    <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.aspx"> <img src="img/logo.png" alt=""> </a>
                        <p>NGOs is expanded to nongovernment organizations. 
                            They are thus a subgroup of all organizations founded by citizens, which include clubs and other associations that provide services, benefits, and premises only to members. 
                            </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Newsletter</h4>
                        <p>Stay updated with our latest trends Seed heaven so said place winged over given forth fruit.
                        </p>
                        <div class="form-wrap" id="mc_embed_signup">
                            <form target="_blank"
                                action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="form-inline">
                                <input class="form-control" name="EMAIL" placeholder="Your Email Address"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
                                    required="" type="email">
                                <button class="btn btn-default text-uppercase"><i class="ti-angle-right"></i></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value=""
                                        type="text">
                                </div>

                                <div class="info"></div>
                            </form>
                        </div>
                        <div class="social_icon">
                            <a href="#"> <i class="ti-facebook"></i> </a>
                            <a href="#"> <i class="ti-twitter-alt"></i> </a>
                            <a href="#"> <i class="ti-instagram"></i> </a>
                            <a href="#"> <i class="ti-skype"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contact us</h4>
                        <p>Faculty of technology and engineering, Near badamani baug , Kalabhavan,Vadodara,390002</p>
                        <div class="contact_info">
                            <p><span class="ti-mobile"></span> +91-8200680204</p>
                            <p><span class="ti-email"></span>MSU.ac.in</p>
                            <p><span class="ti-world"></span>Aashara.com </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->

    <script src="js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="js/swiper.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.smooth-scroll.min.js"></script>
    <!-- swiper js -->
    <script src="js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- contact js -->
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/contact.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>