﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NGO.aspx.cs" Inherits="Aashara.NGO" %>
<!doctype html>
<html lang="en">

<head runat="server">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aashara | NGO</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- magnific popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- nice select CSS -->
    <link rel="stylesheet" href="css/nice-select.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body runat="server">
    <!--::header part start::-->
     <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.aspx"> <img src="img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.aspx">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="NGO.aspx">NGOs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="Gallery.aspx">Gallery</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="Event.aspx">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="Contact.aspx">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="About.aspx">about</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="Login.aspx">Login</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
    </header>
    <!-- Header part end-->
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item text-center">
                            <h2>NGOs</h2>
                            <h3 class="text-white">..Non Government Organizations..</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->
    <!--================Blog Area =================-->
    <section class="blog_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                        <!-- NGO start -->
                        <div id = "NGODiv" runat="server"></div>
                        <!-- NGO End -->
                        <!-- 
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/single_1.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>Founded</h3>
                                    <p>1999</p>
                                </a>
                            </div>
                            
                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>GOONJ</h2>
                                </a>
                                <p>
                                    GOONJ is a NGO headquartered in Delhi,india which undertakes disaster relief,humanitarian aid and community
                                    development in parts of 23 states across india. GOONJ focuses on clothing as a basic but unaddressed need.
                                </p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="far fa-user"></i> Travel, Lifestyle</a></li>
                                    <li><a href="#"><i class="far fa-comments"></i> 03 Comments</a></li>
                                </ul>
                            </div>
                        </article>
                       
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/single_2.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>Founded</h3>
                                    <p>1978</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>HelpAge India</h2>
                                </a>
                                <p>
                                   A leading charity working for the disadvantaged elderly of India, HelpAge India has been active for over four 
                                    decades. It has one of the largest mobile healthcare programs across India, providing free healthcare services to destitute elders.
                                </p>
                                
                            </div>
                        </article>

                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/single_3.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>Founded</h3>
                                    <p>1979</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Google inks pact for new 35-storey office</h2>
                                </a>
                                <p>
                                   CRY(Child Rights and You) was started by Rippan Kapur in 1979 with six of his friends 
                                    and fifty rupees at his mother’s dining table. They had a dream of witnessing a day when every single Indian child would enjoy his/her rights such as survival, protection and development.
                                </p>
                            </div>
                        </article>
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/single_4.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>Founded</h3>
                                    <p>1989</p>
                                </a>
                            </div>
                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Lepra Society</h2>
                                </a>
                                <p>
                                    As the name suggests, Lepra Society works to empower people affected with leprosy. 
                                    It also fosters the healthcare of victims of lymphatic filariasis.
                                </p>
                                
                            </div>
                        </article>

                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/single_5.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>Founded</h3>
                                    <p>2002</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Smile Foundation</h2>
                                </a>
                                <p>
                                    Inspired by the philosophy of Peter Senge, the founder of ‘Society for Organisational Learning’,
                                     a group of young corporate professionals founded Smile Foundation in 2002.
                                </p>
                                
                            </div>
                        </article>  

                        -->

                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Previous">
                                        <i class="ti-angle-left"></i>
                                    </a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link">1</a>
                                </li>
                                <li class="page-item active">
                                    <a href="#" class="page-link">2</a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Next">
                                        <i class="ti-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <!--Search NGO-->
                        <aside class="single_sidebar_widget search_widget">
                            <form action="#">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder='Search Keyword'
                                               onfocus="this.placeholder = ''"
                                               onblur="this.placeholder = 'Search Keyword'">
                                        <div class="input-group-append">
                                            <button class="btn" type="button"><i class="ti-search"></i></button>
                                        </div>
                                    </div>
                                     
                                </div>
                                <button class="button rounded-0 primary-bg text-white w-100 btn_1"
                                        type="submit">
                                    Search
                                </button>
                            </form>
                        </aside>
                        <!--Add New NGO-->
                       <aside class="single_sidebar_widget search_widget">
                        <form action="NGORegister.aspx"><h3>
                         <button class="button rounded-0 w-100 genric-btn success" type="submit">
                                    Add New NGO</button></h3>
                        </form>
                       </aside>
                         <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Category</h4>
                            <ul class="list cat-list">
                                <li>
                                    <a href="#" class="d-flex">
                                        <p>BINGO : </p>
                                        <p>Business-friendly international NGO</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex">
                                        <p>ENGO : </p>
                                        <p>Environmental NGO</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex">
                                        <p>GONGO : </p>
                                        <p>Government-Organized NGO</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex">
                                        <p>INGO : </p>
                                        <p> International NGO</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex">
                                        <p>QUANGO : </p>
                                        <p>Quasi-autononomous NGO</p>
                                    </a>
                                </li>
                            </ul>
                        </aside>
                        <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                        <aside class="single_sidebar_widget instagram_feeds">
                            <h4 class="widget_title">Aashara Feeds</h4>
                            <ul class="instagram_row flex-wrap">
                                <li>
                                    <a href="Gallery.aspx">
                                        <img class="img-fluid" src="img/elements/5.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="Gallery.aspx">
                                        <img class="img-fluid" src="img/elements/4.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="Gallery.aspx">
                                        <img class="img-fluid" src="img/elements/7.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="Gallery.aspx">
                                        <img class="img-fluid" src="img/elements/3.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="Gallery.aspx">
                                        <img class="img-fluid" src="img/elements/2.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="Gallery.aspx">
                                        <img class="img-fluid" src="img/elements/1.jpg" alt="">
                                    </a>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
    <!-- footer part start-->
     <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.aspx"> <img src="img/logo.png" alt=""> </a>
                        <p>NGOs is expanded to nongovernment organizations. 
                            They are thus a subgroup of all organizations founded by citizens, which include clubs and other associations that provide services, benefits, and premises only to members. 
                            </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Newsletter</h4>
                        <p>Stay updated with our latest trends Seed heaven so said place winged over given forth fruit.
                        </p>
                        <div class="form-wrap" id="Div1">
                            <form target="_blank"
                                action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="form-inline">
                                <input class="form-control" name="EMAIL" placeholder="Your Email Address"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
                                    required="" type="email">
                                <button class="btn btn-default text-uppercase"><i class="ti-angle-right"></i></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value=""
                                        type="text">
                                </div>

                                <div class="info"></div>
                            </form>
                        </div>
                        <div class="social_icon">
                            <a href="#"> <i class="ti-facebook"></i> </a>
                            <a href="#"> <i class="ti-twitter-alt"></i> </a>
                            <a href="#"> <i class="ti-instagram"></i> </a>
                            <a href="#"> <i class="ti-skype"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contact us</h4>
                        <p>Faculty of technology and engineering, Near badamani baug , Kalabhavan,Vadodara,390002</p>
                        <div class="contact_info">
                            <p><span class="ti-mobile"></span> +91-8200680204</p>
                            <p><span class="ti-email"></span>MSU.ac.in</p>
                            <p><span class="ti-world"></span>Aashara.com </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer> 
    <!-- jquery plugins here-->

    <script src="js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="js/swiper.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.smooth-scroll.min.js"></script>
    <!-- swiper js -->
    <script src="js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- contact js -->
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/contact.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>