﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public partial class Volunteer : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {   
           // Response.Write("<script>alert('alert 1')</script>");
            if (Session["UserId"] == null)
            {
               // Response.Write("<script>alert('alert 2')</script>");
                Response.Redirect("Login.aspx");
            }
            else
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
                con.Open();
                try
                {
                    SqlCommand cmmd = new SqlCommand("Insert into [Volunteer] (N_Id,E_Id,U_Id) values(@N_Id,@E_Id,@U_Id)", con);
                    cmmd.Parameters.AddWithValue("@N_Id", Request.QueryString["nid"].ToString());
                    cmmd.Parameters.AddWithValue("@E_Id", Request.QueryString["id"].ToString());
                    cmmd.Parameters.AddWithValue("@U_Id", Session["UserId"].ToString());
                    cmmd.ExecuteNonQuery();
                    cmmd.Dispose();
                }
                catch (SqlException ex) { }
                con.Close();
                Response.Write("<script>alert('you become a volunter')</script>");
              //  Response.Redirect("Event.aspx");
            }

        }
    }
}