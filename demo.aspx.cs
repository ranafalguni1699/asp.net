﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Aashara
{



    public class Events
    {
        public int Id { get; set; }
        public int NId { get; set; }
        public string Name { get; set; }
        public string Loc { get; set; }
        public DateTime Sdate { get; set; }
        public DateTime Edate { get; set; }
        public DateTime Now { get; set; }
        public TimeSpan Difference { get; set; }
        public int SortValue { get; set; }
        public Events(int E_Id, int N_Id, string E_Name , DateTime E_Sdate, DateTime E_Edate, string Location, string description, bool reminder)
        {
            Id = E_Id;
            NId = N_Id;
            Name = E_Name;
            Loc = Location;
            Sdate = E_Sdate;
            Edate = E_Edate;
            Now = DateTime.Now;
            
            Difference = E_Sdate - Now;
            SortValue = Convert.ToInt32(Difference.Days % 365);
            if (SortValue < 0) { SortValue += 365; }

        }
    }
    public partial class demo : System.Web.UI.Page
    {
        SqlConnection con;

        protected void Page_Load(object sender, EventArgs e){
            

        }

        protected void Submit_Click(Object sender, EventArgs e)
        {
             con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            //String connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\ASPNet\Aashara\Aashara\App_Data\Ashara.mdf;Integrated Security=True";
            String output="   ";
            //con = new SqlConnection(connectionString);

            con.Open();


//        string strSql = "SELECT COUNT(*) FROM Volunteer WHERE E_Id=' " + ds.Tables[0].Rows[i]["E_Id"] + "'";
//          SqlCommand command = new SqlCommand(strSql, con);
//            int count = Convert.ToInt32(command.ExecuteScalar());
//           lblStatus.Text = Convert.ToString(count);

            SqlCommand cmd = new SqlCommand("select * from Admin", con);
            SqlDataReader dataReader;
            dataReader = cmd.ExecuteReader();
            String name, pass;
            name = uname.Text.ToString();
            pass = pswd.Text.ToString();
            while (dataReader.Read())
            {
                if (name.Equals(dataReader.GetValue(2)) && pass.Equals(dataReader.GetValue(3)))
                { 
//                    Response.Write("<h4>Login Succesfully </h4>");
                    Response.Redirect("index.aspx");
                }
            }
            dataReader.Close();
            cmd.Dispose();
            con.Close();
        }
    }
}