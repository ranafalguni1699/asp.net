﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public partial class NGORegister : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        { }
        protected void Submit_Click(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            try
            {
                if (NGOImg.HasFile)
                {
                    if ((NGOImg.PostedFile != null) && (NGOImg.PostedFile.ContentLength > 0))
                    {
                        string fn = System.IO.Path.GetFileName(NGOImg.PostedFile.FileName);
                        string SaveLocation = Server.MapPath("src/NGOImage/") + "\\" + fn;
                        try{  NGOImg.PostedFile.SaveAs(SaveLocation);  }
                        catch (Exception ex)  { }
                    }
                }

                if (NGOCertificate.HasFile)
                {
                    if (NGOCertificate.PostedFile != null)
                    {
                        string fn = System.IO.Path.GetFileName(NGOCertificate.PostedFile.FileName);
                        string SaveLocation = Server.MapPath("src/NGOCertificate/") + "\\" + fn;
                        try { NGOCertificate.PostedFile.SaveAs(SaveLocation); }
                        catch (Exception ex) { }
                    }
                }
                //Response.Write("<script>alert('hello')</script>");
                SqlCommand cmd = new SqlCommand("Insert into [NGO] (N_Name,N_Email,N_Password,N_Address,N_Contact,N_Description,N_Img,N_Certificate) values(@N_Name,@N_Email,@N_Password,@N_Address,@N_Contact,@N_Description,@N_Img,@N_Certificate)", con);
                cmd.Parameters.AddWithValue("@N_Name", NGOName.Text);
                cmd.Parameters.AddWithValue("@N_Email", NGOEmail.Text);
                cmd.Parameters.AddWithValue("@N_Password", NGOPswd.Text);
                cmd.Parameters.AddWithValue("@N_Address", NGOAddress.Text);
                cmd.Parameters.AddWithValue("@N_Contact", NGOContact.Text);
                cmd.Parameters.AddWithValue("@N_Description", NGODescription.Text);
                cmd.Parameters.AddWithValue("@N_Img", NGOImg.PostedFile.FileName);
                cmd.Parameters.AddWithValue("@N_Certificate", NGOCertificate.PostedFile.FileName);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                Response.Redirect("NGOList.aspx");
            }
            catch (SqlException ex)
            { }


        }
        protected void Reset_Click(object sender, EventArgs e)
        {
            Response.Redirect("NGO.aspx");
        }
    }
}