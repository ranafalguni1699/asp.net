﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//Aspx :
//<div id = "DIV1" runat="server"></div>
//Code behind :
//DIV1.InnerHtml = "some text";

namespace Aashara
{
    public partial class NGO : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            String str = " ";
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from NGO", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cmd.ExecuteNonQuery();
            con.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    str += "<article class='blog_item'>";
                    str += " <div class='blog_item_img'>";
                    str += "  <img class='card-img rounded-0' src='src/NGOImage/" + ds.Tables[0].Rows[i]["N_Img"] + "'>";
                    str += "  <a class='blog_item_date'><h3>" + ds.Tables[0].Rows[i]["N_Name"];
                    str += "  </h3></a>";
                    str += " </div> ";
                    str += " <div class='blog_details'>";
                    str += "  <a class='d-inline-block'><h2> ";
                    str += ds.Tables[0].Rows[i]["N_Email"] + " </h2></a>";
                    str += "  <p>" + ds.Tables[0].Rows[i]["N_Description"] + " description </p>";
                    str += "  <ul class='blog-info-link'>";
                    str += "   <li><a><i class='far fa-user'></i> ";
                    str += ds.Tables[0].Rows[i]["N_Contact"];
                    str += "   </a></li>";
                    str += "   <li><a><i class='far fa-comments'></i>";
                    str += ds.Tables[0].Rows[i]["N_Address"];
                    str += "   </a></li>";
                    str += "  </ul>";
                    str += " </div>";
                    str += "</article> ";
                }
            }
            NGODiv.InnerHtml = str;
            con.Close();
        }
    }
}