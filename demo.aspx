﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="demo.aspx.cs" Inherits="Aashara.demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           <h3>Login Form</h3>
        <h4><label id="namelbl">UserName </label></h4>
        <asp:TextBox ID="uname" runat="server"> </asp:TextBox>
        <h4><label id="pswdlbl">Password </label>    </h4>
        <asp:TextBox ID="pswd" runat="server" TextMode="Password"> </asp:TextBox>
        <br /><br /><asp:Button ID="submit" runat="server" Text="Submit" OnClick="Submit_Click" />
        </div>
    </form>
</body>
</html>
