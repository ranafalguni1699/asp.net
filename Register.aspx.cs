﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public partial class Signin : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
           // Response.Write("<script>alert('Hello Reload')</script>");
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            try
            {
                Response.Write("<script>alert('hello')</script>");
                SqlCommand cmd = new SqlCommand("Insert into [User] (U_Name,U_Email,U_Password,U_DOB,U_gender,U_Contact_No,U_Address,U_City,U_State) values(@U_Name,@U_Email,@U_Password,@U_DOB,@U_gender,@U_Contact_No,@U_Address,@U_City,@U_State)", con);
                cmd.Parameters.AddWithValue("@U_Name", fname.Text + " " + lname.Text);
                cmd.Parameters.AddWithValue("@U_Email", email.Text);
                cmd.Parameters.AddWithValue("@U_Password", pswd.Text);
                cmd.Parameters.AddWithValue("@U_DOB", dob.Text);
                cmd.Parameters.AddWithValue("@U_gender", gender.Text);
                cmd.Parameters.AddWithValue("@U_Contact_No", contact.Text);
                cmd.Parameters.AddWithValue("@U_Address", address.Text);
                cmd.Parameters.AddWithValue("@U_City", city.Text);
                cmd.Parameters.AddWithValue("@U_State", state.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                Response.Redirect("Login.aspx");
            }
            catch (SqlException ex)
            { }
            //Response.Write("<script>alert('Hello1')</script>");
        }
        protected void Reset_Click(object sender, EventArgs e)
        {
           // Response.Write("<script>alert('Hello1')</script>");
            Response.Redirect("Signin.aspx");
        }
    }
}