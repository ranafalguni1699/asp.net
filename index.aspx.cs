﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Aashara
{
    public partial class index : System.Web.UI.Page
    {
        int count=0;
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("Visitor",count);
            count += 1;
            String str = " ";
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            try
            {
                try
                {
                    SqlCommand cmd1;
                    SqlDataReader dr;
                    int count = 0;
                    //int count2 = 0;
                    cmd1 = new SqlCommand("select * from NGO", con);
                    dr = cmd1.ExecuteReader();
                    while (dr.Read()) { count += 1; }
                    NCount.Text = count.ToString();
                    dr.Close();
                    cmd1.Dispose();
                }
                catch (Exception ex) { }

                try
                {
                    SqlCommand cmd2;
                    SqlDataReader dr;
                    int count = 0;
                    cmd2 = new SqlCommand("select * from Event", con);
                    dr = cmd2.ExecuteReader();
                    while (dr.Read()) { count += 1;}
                    Ecount.Text = count.ToString();
                    dr.Close();
                    cmd2.Dispose();
                }
                catch (Exception ex) { }
                try
                {
                    SqlCommand cmd3;
                    SqlDataReader dr1;
                    int count = 0;
                    cmd3 = new SqlCommand("select * from Volunteer", con);
                    dr1 = cmd3.ExecuteReader();
                    while (dr1.Read()) { count += 1; }
                    VCount.Text = count.ToString();
                    dr1.Close();
                    cmd3.Dispose();
                }
                catch (Exception ex) { }
            }
            catch (Exception ex) { }

            SqlCommand cmd = new SqlCommand("select * from Event", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cmd.ExecuteNonQuery();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i<3 ; i++)
                {
                    str += "<div class='col - sm - 6 col - lg - 4 col - xl - 4'> ";
                    str += " <div class='single - home - passion'> ";
                    str += "  <div class='card'>";
                    str += "   <img src = 'AdminPanel/Upload/" + ds.Tables[0].Rows[i]["E_Img"]  + "' class='card - img - top' alt='blog'>";
                    str += "   <div class='card-body'> <a href = 'passion.html'> <h5 class='card-title'>";
                    str += ds.Tables[0].Rows[i]["E_Name"] + " </h5></a><br/>";
                    str += " <ul><li><img src = 'img/icon/passion_2.svg'> From";
                    str += " <b>" + Convert.ToDateTime(ds.Tables[0].Rows[i]["E_Sdate"]).ToString("MM/dd/yyyy") + "</b> To <b>";
                    str += Convert.ToDateTime(ds.Tables[0].Rows[i]["E_Edate"]).ToString("MM/dd/yyyy") + "</b> </li><li> ";
                    str += "<img src = 'img/icon/passion_1.svg'> Location: <b>" + ds.Tables[0].Rows[i]["E_Location"];
                    str += " </b></li></ul><br/>";
                    str += "<a href='Event.aspx' class='btn_3'>Read More </a>";
                    //str += "<a onClick='Page_Load' class='btn_3'>Become Volunteer </a>";
                    Session["eid"] = ds.Tables[0].Rows[i]["E_Id"];
                    Session["nid"] = ds.Tables[0].Rows[i]["NGO_Id"];
                    str += "   </div>";
                    str += "  </div> ";
                    str += " </div> ";
                    str += "</div> ";
                }
                EventDiv.InnerHtml = str;
                con.Close();
            }

        }
    }
}