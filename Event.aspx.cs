﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public class EventSort
    {
        public int Id { get; set; }
        public int NId { get; set; }
        public string Name { get; set; }
        public string Loc { get; set; }
        public DateTime Sdate { get; set; }
        public DateTime Edate { get; set; }
        public DateTime Now { get; set; }
        public TimeSpan Difference { get; set; }
        public int SortValue { get; set; }

        public EventSort(int E_Id, int N_Id, string E_Name, DateTime E_Sdate, DateTime E_Edate, string Location, string description, bool reminder)
        {
            Id = E_Id;
            NId = N_Id;
            Name = E_Name;
            Loc = Location;
            Sdate = E_Sdate;
            Edate = E_Edate;
            Now = DateTime.Now;

            Difference = E_Sdate - Now;
            SortValue = Convert.ToInt32(Difference.Days % 365);
            if (SortValue < 0) { SortValue += 365; }

        }
    }

    public partial class Event : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            String str = " ";
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Event order by E_Sdate", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cmd.ExecuteNonQuery();
            if (ds.Tables[0].Rows.Count > 0)
            {
                //str += "<table> ";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if((i%3)==0){ str += "<div class='row'> "; }
                  //      str += "<td> ";
                        str += "<div class='col - sm - 6 col - lg - 4 col - xl - 4'> ";
                        str += " <div class='single - home - passion'> ";
                        str += "  <div class='card'>";
                        str += "   <img src = 'AdminPanel/Upload/" + ds.Tables[0].Rows[i]["E_Img"] + "' class='card - img - top' alt='blog'>";
                        //str += "   <img src = 'img/passion/passion_1.png' class='card - img - top' alt='blog'>";
                        str += "   <div class='card-body'> <a href = 'passion.html'> <h5 class='card-title'>";
                        str += ds.Tables[0].Rows[i]["E_Name"] + " </h5></a><br/>";
                        str += " <ul><li><img src = 'img/icon/passion_2.svg'> From";
                        str += " <b>" + Convert.ToDateTime(ds.Tables[0].Rows[i]["E_Sdate"]).ToString("MM/dd/yyyy") + "</b> To <b>";
                        str += Convert.ToDateTime(ds.Tables[0].Rows[i]["E_Edate"]).ToString("MM/dd/yyyy") + "</b> </li><li> ";
                        str += "<img src = 'img/icon/passion_1.svg'> Location: <b>" + ds.Tables[0].Rows[i]["E_Location"];
                        str += " </b></li></ul><br/>";
                        str += "<a href='Volunteer.aspx?id=" + ds.Tables[0].Rows[i]["E_Id"] + "&nid=" + ds.Tables[0].Rows[i]["NGO_Id"] + "' class='btn_3'>Become Volunteer </a>";
                        //str += "<a onClick='Page_Load' class='btn_3'>Become Volunteer </a>";
                        //  Session["eid"] = ds.Tables[0].Rows[i]["E_Id"];
                        //Session["nid"] = ds.Tables[0].Rows[i]["NGO_Id"];
                        str += "   </div>";
                        str += "  </div> ";
                        str += " </div> ";
                        str += "</div> ";
                    //    str += "</td> ";
                   if ((i%3)==0){      str += "</div> ";     }       
                }
                str += "</table> ";
                EventDiv.InnerHtml = str;
                con.Close();
            }
        }
    }
}