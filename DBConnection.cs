﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Aashara
{
    public class DBConnection
    {
        SqlConnection cnn;
        string connectionString;

        public void connection()
        {   
            connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\ASPNet\Aashara\Aashara\App_Data\Ashara.mdf;Integrated Security=True";

            cnn = new SqlConnection(connectionString);
            try
            {
                if(cnn.State== ConnectionState.Closed)
                {
                    cnn.Open();

                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}