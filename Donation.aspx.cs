﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aashara
{
    public partial class Donation : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            string com = "Select * from NGO";
            SqlDataAdapter adpt = new SqlDataAdapter(com, con);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            NGOList.DataSource = dt;
            NGOList.DataBind();
            NGOList.DataTextField = "N_Name";
            NGOList.DataValueField = "N_Id";
            NGOList.DataBind();
            con.Close();
        }

        protected void Donation_Click(object sender, EventArgs e)
        {

            // Response.Write("<script>alert('hello10')</script>");
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ashara"].ConnectionString);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Insert into [Donation] (N_Id,U_Id,Rupees) values(@N_Id,@U_Id,@Rupees)", con);
                cmd.Parameters.AddWithValue("@N_Id", NGOList.Text);
                cmd.Parameters.AddWithValue("@U_Id", Session["UserId"]);
                cmd.Parameters.AddWithValue("@Rupees", Rupees.Text);
                cmd.ExecuteNonQuery();
                //Response.Write("<script>alert('hello1')</script>");
                cmd.Dispose();
                Response.Redirect("index.aspx");
            }
            catch (SqlException ex)
            { }
            con.Close();


        }
    }
}